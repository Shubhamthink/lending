const express = require("express");
const router = express.Router();
const Home = require("./prosper");
const Login = require("./login");



router.use("/", Home);
router.use("/", Login);

module.exports = router;
