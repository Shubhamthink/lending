const express    = require("express");
const router     = express.Router();
const HomeLayout = require("../controller/Home")
const isAuth = require("../config/authenticate");


router.get("/" , HomeLayout.HomePage)
router.get("/apply" , HomeLayout.FormLayout)
router.post("/apply" , HomeLayout.CreateData)

router.get("/dashboard" , isAuth.isAuth ,HomeLayout.ListingData)

router.get("/update/:id", isAuth.isAuth  ,  HomeLayout.EditData);

router.get('/delete/:id', isAuth.isAuth  ,  HomeLayout.DeleteData)
router.get('/delete', isAuth.isAuth  ,  HomeLayout.DeleteAll)



// router.post("/dashboard" , isAuth.isAuth ,HomeLayout.ListingData)

// router.get("/Thankyou", (req,res) => {
//     res.render("thankyou")
// })


// function ensureAuthenticated(req, res, next) {
//     if (req.isAuthenticated()) {
//       return next();
//     } else {
//       req.flash("danger", "Please login");
//       res.redirect("/login");
//     }
//   }

module.exports = router;