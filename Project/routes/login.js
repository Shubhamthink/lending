const express    = require("express");
const router     = express.Router();
const Login = require("../controller/Login")


router.get("/login" ,  Login.getLogin)


router.post("/login" , Login.login)

router.get("/change_password", Login.change_password)

router.post("/change_password", Login.createPassword)


module.exports = router;