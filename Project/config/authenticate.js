function 
ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash("danger", "Please login");
    res.redirect("/login");
  }
}

module.exports.isAuth = ensureAuthenticated;