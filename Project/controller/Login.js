const db = require("../config/Db");
const Sequelize = require("sequelize");
const Login = require("../model/Login");
const bcrypt = require("bcryptjs");
const passport = require("passport");
const { validationResult } = require("express-validator");

exports.getLogin = (req, res, next) => {
  res.render("login", {
    pagetitle: "Login",
    errors: false,
    form: false,
    req: req,
    layout: "Login",
  });
};




exports.login = (req, res, next) => {
  var form = {
    email: req.body.email,
  };
  const errors = validationResult(req);

  console.log("ERRORS", errors);
  var er = {};
  for (var i = 0; i < errors.errors.length; i++) {
    var key = errors.errors[i].param;
    er[key] = errors.errors[i].msg;
  }
  console.log("er : ", er);

  if (!errors.isEmpty()) {
    console.log("ERRORS in if ", errors.errors);
    return res.render("login", {
      errors: er,
      form: form,
      req: req,
      layout: "Login",
    });
  } else {
    passport.authenticate("local", {
      successRedirect: "/dashboard",
      failureRedirect: "/login",
      failureFlash: true,
    })(req, res, next);
  }
};

exports.change_password = (req, res, next) => {
  res.render("changePassword", {
    pagetitle: "Change Password",
    errors: false,
    form: false,
    req: req,
    layout: "Change Password",
  });
};

exports.createPassword = async (req, res, next) => {
  try {

    var findEmailAndPassword = await Login.findOne({
      where: {
        email: req.body.email
      }, raw: true, nest: true
    })

    if (req.body.newpassword == "") {
      return res.json({
        message: `New Password is required`,
        status: 301,
      });
    }

    if (findEmailAndPassword) {
      if (findEmailAndPassword.password == req.body.oldpassword) {
        var updatePassword = await Login.update(
          { password: req.body.newpassword }, { where: { id: findEmailAndPassword.id } })

        if (updatePassword) {
          return res.json({
            message: `Password update SuccessFully`,
            status: 200,
          });
        }


      } else {
        return res.json({
          message: `Old Password is not match!`,
          status: 403,
        });
      }
    } else {
      return res.json({
        message: `Email does not match!`,
        status: 403,
      });
    }


  } catch (error) {

  }
}
