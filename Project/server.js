const express = require('express')
const app = express();
const path = require("path")
const routes = require('./routes/MainRoute')
var bodyParser = require('body-parser')
const ejs = require("ejs")
const passport = require("passport");
var session = require('express-session')

const port = "4000";



var a = require("../Project/config/Db")
console.log(a , ":a");

const staticpath = path.join(__dirname, "public")
app.use(express.static(staticpath))
app.use(bodyParser.json({ limit: "50mb" }))
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }))

const flash = require('connect-flash');
const profileRoutes = require('./routes/login');

app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
}))

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(function (req, res, next) {
    console.log("session flash : ", req.session)
    res.locals.sessionFlash = req.session.flash;
    delete req.session.flash;
    next() // otherwise continue
})
app.get('*', (req, res, next) => {
    res.locals.user = req.user || null;
    // console.log(res.locals.user  , ": hyy");

    next();
})
require("./config/passport")(passport);

const partials_path = path.join(__dirname, "templates/partials");
app.use("/static", express.static("public"))
app.set("views", path.join(__dirname, "templates/views"))
app.set('view engine', 'ejs');
app.use("/", routes);

app.listen(port, (req, res) => {
    console.log(`Create Server on  ${port} PORT number`);

})
