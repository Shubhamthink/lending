const mongoose = require('mongoose');

const loanFormSchema = new mongoose.Schema(
  {
    ddlServices: {
      type: String,
    },
    first_name: {
      type: String,
    },
    middle_name: {
      type: String,
    },
    last_name: {
      type: String,
    },
    email: {
      type: String,
    },
    number: {
      type: Number,
    },
    dob: {
      type: String,
    },
    address: {
      type: String,
    },
    city: {
      type: String,
    },
    state: {
      type: String,
    },
    Zip: {
      type: String,
    },
    driving_licence: {
      type: String,
    },
    driving_licence_state: {
      type: String,
    },
    ssn: {
      type: Number,
    },
    routing: {
      type: String,
    },
    bank_name: {
      type: String,
    },
    AC_number: {
      type: String,
    },
    mobile_onuserid: {
      type: String,
    },
    mobile_onlinepassword: {
      type: String,
    },
  },
  { timestamps: false }
);
const LoanForm = mongoose.model('LoanForm', loanFormSchema);

module.exports = LoanForm;
