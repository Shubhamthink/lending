const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: false // Change this if you want uniqueness constraints
    },
    password: {
        type: String,
        default: '-' // Setting a default value
    }
}, { timestamps: false });

const User = mongoose.model('User', userSchema);

module.exports = User;
